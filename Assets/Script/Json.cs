﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Json : MonoBehaviour
{
    void Start()
    {
        WWW req = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(json(req));
    }

    IEnumerator json(WWW request)
    {
        yield return request;
        Debug.Log(request.text);
    }

   public string getjson(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[0]["name"].Value;
    }


    // Update is called once per frame
    void Update()
    {


    }
}
