﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class TextScript : MonoBehaviour
{
    [SerializeField] Text nama;
    [SerializeField] Text email;


    // Start is called before the first frame update
    void Start()
    {
        WWW req = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(json(req));
    }


    IEnumerator json(WWW request)
    {
        yield return request;
        Debug.Log(getName(request.text));
        nama.text = getName(request.text);
        email.text = getEmail(request.text);
    }

    public string getName(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[0]["name"].Value;
    }

    public string getEmail(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[1]["email"].Value;
    }
}

public class PersonData
{
    public int id;
    public string name;
    public string avatar;
    public string email;

    public PersonData(int id, string name, string avatar, string email)
    {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.email = email;
    }
}


